/**
 * Created by wuja on 14/10/2016.
 */
/* global angular, _ */
angular.module("heyMisterHangman", [])
	.controller("ctrlOne", function($scope, GameService, $timeout) {
		// 0. defs & cfg
		$scope.checkForDupes = true;
		$scope.gameLoading = true;
		$scope.loadingErrors = false;
		$scope.gameSession = {
			goodGuesses: [],
			badGuesses: []
		};

		// 1. methods
		var gameLogic = {
				checkCharOccurence: function (char) {
					console.log('occurence of:',char, $scope.gameSession.currentPassword, $scope.gameSession.currentPassword.indexOf(char));
					if($scope.gameSession.currentPassword.indexOf(char) == -1) {
						if(gameLogic.checkForDupes(char) && $scope.checkForDupes) {
							$scope.gameSession.badGuesses.push(char);
						}
						$scope.gameSession.lastGuess = "bad";
						$timeout(function () {
							$scope.gameSession.lastGuess = false;
						}, 450);
					} else {
						if(gameLogic.checkForDupes(char) && $scope.checkForDupes) {
							$scope.gameSession.goodGuesses.push(char);
						}
						$scope.gameSession.lastGuess = "ok";
						$timeout(function () {
							$scope.gameSession.lastGuess = false;
						}, 450);
					}
				},
				checkForDupes: function (char) {
					return $scope.gameSession.badGuesses.indexOf(char) == -1 && $scope.gameSession.goodGuesses.indexOf(char) == -1;
				},
				setupLetters: function (password) {
					var n = password.toLowerCase().split("");
					$scope.gameSession.dupeFreePassword = _.uniq(n);
					while (n.length < 11) n.unshift("");
					return n;
				},
				letterShown: function (char) {
					return $scope.gameSession.goodGuesses.indexOf(char) != -1;
				}
			},
			gameHandlers = {
				gameOver: function () {
					$scope.gameSession.gameOver = true;
				},
				gameWon: function () {
					$scope.gameSession.gameWon = true;
				},
				newGame: function () {
					$scope.gameLoading = true;
					$scope.gameSession.gameOver = false;
					$scope.gameSession.gameWon = false;
					GameService.getNewWord().then(function ok (res) {
						$scope.gameSession = GameService.getNewGameInstance(res.data.word);
						$scope.gameSession.preparedLetters = gameLogic.setupLetters(res.data.word);
						$scope.gameLoading = false;
					}, function err () {
						$scope.loadingErrors = true;
						$scope.gameLoading = false;
					});
				}
			};

		$scope.gameHandlers = gameHandlers;
		$scope.letterShown = gameLogic.letterShown;

		$scope.kPress = function($event){
			// console.log(String.fromCharCode($event.which), $event);
			gameLogic.checkCharOccurence(String.fromCharCode($event.which).toLowerCase());
		};

		// 2. watchers
		$scope.$watch(
			"gameSession.badGuesses.length",
			function (newval) {
				if(!_.isEmpty($scope.gameSession.currentPassword)) {
					if(newval >= 11) gameHandlers.gameOver();
				}
			}
		);

		$scope.$watch(
			"gameSession.goodGuesses.length",
			function (newval) {
				if(!_.isEmpty($scope.gameSession.currentPassword)) {
					if(newval >= $scope.gameSession.dupeFreePassword.length) gameHandlers.gameWon();
				}
			}
		);

		// 3. init
		gameHandlers.newGame();
	})
	.factory("GameService", function ($http) {
		var gameInstance = function (password) {
			this.badGuesses = [];
			this.goodGuesses = [];
			this.currentPassword = password;
			this.gameOver = false;
			this.gameWon = false;
		};

		return {
			getNewGameInstance: function (password) {
				return new gameInstance(password);
			},
			getNewWord: function () {
				return $http({
					method: "GET",
					url: "http://api.wordnik.com:80/v4/words.json/randomWord?hasDictionaryDef=true&minCorpusCount=0&maxCorpusCount=-1&minDictionaryCount=1&maxDictionaryCount=-1&minLength=5&maxLength=11&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5"
				});
			}
		};
	});
